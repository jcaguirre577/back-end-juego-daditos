# nodito
- Backend Juego de dados multijugador 

## Objectivo
- Este proyecto muestra Express + Swagger + Serverless + Lambda + Dynamondb + ClaudiaJS
- Express se utiliza como marco de back-end
- ClaudiaJS se utiliza para implementar el código Express en una arquitectura sin servidor en AWS API  Gateway con tecnología Lambda
- Swagger para documentación (duh) documentación dentro del propio código para que siempre pueda estar actualizado
- Dynamondb como bd no Sql en la nube

### Instalación

```bash
npm install nodito
```

## Librerias Usadas
- `swagger-jsdoc`
- `swagger-ui-express`
- `claudia`
- `aws-serverless-express`
- `node-fetch`
-  Lambdas
-  aws-serverless-express
-  underscore
-  express

### Base de datos AWS DynamoDB

Integración: la integración de esta base e datos no sql que nos proporciona aws lo podemos hacer de 2 formar ingresando al tableblero en aws y buscamos DynamoDB creariamos la bd con sus colecciones, pero en este proyecto estoy usando ( SERVERLESS ) serverless nos da la ayuda de crear nuestra bd en el archivo de configuración ( Serverless.yml ) creando asi  nombre db, coleccciones para asi poder subir ya todo listo al aws al ejecutar el comando ya antes mencionado (serverless deploy --verbose )  